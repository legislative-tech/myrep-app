# MyRep.app 

This application is intended to help map a set of priorities from a district full of people into a limited set of representatives. 

## Manifesto

1.  Representative democracy is a good thing.
2.  In the United States, the single-rep-per-district format for most state and federal House of Representatives districts leads to a 2-party system
3.  The two parties have created primaries and taken other steps to limit additonal parties
4.  First-past-the-post voting leads to people mainly voting "against" the biggest threat despite not agreeing with much of the vote recipient
5.  If a group of 3-5 representatives existed per district (3-5x larger) the two major parties could field their candidates and additional parties could be represented as well
6.  Even better would be a ranked choice system where candidates that don't do well revert their votes to the remainder of candidates by tracing the next highest ranked per ballot
7.  Since districts would be larger and representation isn't a binary choice of "my rep is my party and thus I feel represented" or "not", a mediator would help
8.  Locally elecgted officials are ignored 90% of the time and very poorly advance the general agenda of their district, mainly focusing on one area that they are passionate about
9.  Be the change you want to see in the world

# This Repo

The repo is configured to put as much hygiene as possible up front. Each commit has a type check and linting component. 

Test coverage is also important so a pipeline job should keep track of that. 

## Getting Started

### The full development experience

External things:

1.  A publicly accessible domain name with SSL for SSO
1.  A gitlab account with an oauth application key for SSO
1.  A honeycomb.io account (free tier seems fine) with a team, get that API key
1.  A twitter application for Twitter SSO
1.  An email sender thing like AWS Simple Email Service for email SSO

1.  Run `cp .env.sample .env.local` and load it up with the real stuff above 
1.  Run `yarn dev` to install and run everything.

### Development experience

Setup the accounts you want to use in the .env.local and then comment out auth mechanisms you don't want to do in: `pages/api/auth/[...nextauth].ts`

If you remove the `HONEYCOMB_*` tracing, also remove the `NODE_OPTIONS` from `package.json`'s `dev` command so that `tracing.js` isn't called.

If you want honeycomb, make sure to set the following environment variables:

```bash
export HONEYCOMB_DATASET=myrep-app
export HONEYCOMB_TEAM=[[SOME KEY FROM HONEYCOMB]]
```

Will work on a way to mock auth locally so developers don't need to register a gitlab or twitter application.

This will set environment variables from your .env.local file and then `yarn dev` to start the dev service. If you change the environment variables in .env.local, you'll have to unset and/or re-export them. 

```bash
yarn dev
```

## Docker build

```
docker build -t myrep:local .
docker create --name myrep -p 3000:3000 --env-file .env.local myrep:local
docker start myrep
```

### Contributing guidelines

Try not to make the test case coverage worse. Let Husky do pre-commit linting and pre-push testing. If things look bad and husky complains, fix it the best you can. 

# Original Readme from Next.js

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
