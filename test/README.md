# Test directory

This directory exists to provide a safe place to create unit tests for the `./pages` directory. Since NextJS makes routes out of all the files in there, the test files could be deployed and accessible. This should move them out of harm's way. 

As soon as there's a better way to handle it, this will go away. 