import renderer from 'react-test-renderer'
import DistrictIssues from '../pages/district-issues'

describe('Home page', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<DistrictIssues />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
