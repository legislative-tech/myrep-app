import renderer from 'react-test-renderer'
import BeHeard from '../pages/be-heard'

describe('Home page', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<BeHeard />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
