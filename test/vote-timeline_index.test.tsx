import renderer from 'react-test-renderer'
import VoteTimeline from '../pages/vote-timeline'

describe('Home page', () => {
  it('should match the snapshot', () => {
    const tree = renderer.create(<VoteTimeline />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
