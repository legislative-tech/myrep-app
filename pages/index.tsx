import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import { signIn, signOut, useSession } from 'next-auth/client'
import opentelemetry from '@opentelemetry/api'

export default function Home() {
  const [session] = useSession()
  const span = opentelemetry.trace.getTracer('default').startSpan('home')
  span.setAttribute('tier', 'frontend')

  const handleLogin = (e) => {
    e.preventDefault()
    span.setAttribute('action', 'login')
    signIn()
  }

  const handleLogout = (e) => {
    e.preventDefault()
    span.setAttribute('action', 'logout')
    signOut()
  }
  span.end()

  return (
    <div className={styles.container}>
      <Head>
        <title>MyRep.app</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>MyRep.app</h1>

        <p className={styles.description}>Be heard; disagree politely; participate in democracy.</p>

        <div className={styles.grid}>
          <>
            {!session?.user ? (
              <a className={styles.card} href="#" onClick={handleLogin}>
                <h3>Login &rarr;</h3>
                <p>For access to district information and participation options.</p>
              </a>
            ) : (
              <span>&nbsp;</span>
            )}
          </>

          <Link href="/be-heard">
            <a className={styles.card}>
              <h3>Be Heard &rarr;</h3>
              <p>Express yourself to your representatives.</p>
            </a>
          </Link>
          <Link href="/district-issues">
            <a className={styles.card}>
              <h3>District Issues &rarr;</h3>
              <p>Learn the current pressing issues for your district and weigh in.</p>
            </a>
          </Link>
          <Link href="/vote-timeline">
            <a className={styles.card}>
              <h3>Vote Timeline &rarr;</h3>
              <p>See upcoming legislation and review previous votes.</p>
            </a>
          </Link>
          <a
            href="https://gitlab.com/legislative-tech/myrep-app"
            target="_blank"
            rel="noreferrer"
            className={styles.card}
          >
            <h3>Improve MyRep.app &rarr;</h3>
            <p>Participate in development of this system.</p>
          </a>
          <>
            {session?.user ? (
              <a className={styles.card} href="#" onClick={handleLogout}>
                <h3>Logout &darr;</h3>
                <p>Thanks and come back soon!</p>
              </a>
            ) : (
              <span>&nbsp;</span>
            )}
          </>
        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
        |<Link href="privacy-policy">Privacy Policy</Link>|
        <Link href="terms-of-service">Terms of Service</Link>
      </footer>
    </div>
  )
}
