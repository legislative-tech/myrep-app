import Head from 'next/head'
import Link from 'next/link'
import styles from '../../styles/Page.module.css'
import { useSession, getSession } from 'next-auth/client'
import { useState } from 'react'
import opentelemetry from '@opentelemetry/api'

export default function BeHeard() {
  const [session, loading] = useSession()
  const span = opentelemetry.trace.getTracer('default').startSpan('be-heard')
  const [story_textarea, setStory_textarea] = useState('')
  const [formresponse, setFormresponse] = useState('')
  const [stories, setStories] = useState([
    // replace with get initial props or something...
    { id: 1, author: 'jim', story: 'I ate too much ham.' },
    { id: 2, author: 'bob', story: 'Not enough time for ham.' },
  ])
  span.setAttribute('myrep.tier', 'frontend')

  if (typeof window !== 'undefined' && loading) return null

  const tell_story = async (e) => {
    e.preventDefault() // prevents page reload
    span.setAttribute('myrep.action', 'tell_story')
    if (story_textarea.length < 1) {
      alert('Blank story. Please write something.')
      return
    }
    try {
      span.setAttribute('myrep.endpoint', 'api/be-heard')
      const res = await fetch('./api/be-heard', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          story: story_textarea,
        }),
      })
      span.setAttribute('myrep.endpoint-response-status', res.status)
      const json_data = await res.json()
      if (res.status === 200) {
        setFormresponse('Thank you, all ' + json_data.word_count + ' words were received.')
        setStories(json_data.stories)
        setStory_textarea('')
      } else {
        setFormresponse('Submission problem.')
      }
    } catch (err) {
      alert('Sorry, something went wrong before sending.')
      span.setAttribute('myrep.caught-error', err.message)
    }
  }
  span.end()

  return (
    <div className={styles.container}>
      <Head>
        <title>Be Heard by my rep</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Link href="/">&larr; Home</Link>

        <h1 className={styles.title}>Be Heard</h1>
        <div className="friendlyblock">
          {session ? (
            <>
              <p>Form to enter your grievances</p>
              <h2>Hey {session?.user.name}!!</h2>
              <Link href="/api/auth/session">
                Want to view your full JSON Session? Click here &rarr;
              </Link>
            </>
          ) : (
            <>
              <p>
                The <span className={styles.code}>Be Heard</span> section is a forum for expressing
                ideas.
              </p>
              <p>
                <Link href="/">Login to be heard!</Link>
              </p>
            </>
          )}
          <p>Are you suffering?</p>
          <p>Is your community suffering?</p>
          <p>What of those are regular struggles and which need legislative action to correct?</p>
        </div>

        {session ? (
          <>
            <div>
              <form>
                <label htmlFor="story_field">What&rsquo;yarn s your story?</label>
                <textarea
                  id="story_field"
                  name="story"
                  placeholder="Share your experiences"
                  onChange={(e) => setStory_textarea(e.target.value)}
                  value={story_textarea}
                  rows={10} // eslint-disable-line no-eval
                  cols={50} // eslint-disable-line no-eval
                ></textarea>
                <button type="submit" onClick={tell_story}>
                  Submit
                </button>
              </form>
              {formresponse}
            </div>
          </>
        ) : (
          <>&nbsp;</>
        )}
        <hr />
        <ol>
          {stories.map((tale) => (
            <li key={tale.id}>
              {tale.story} <br />- {tale.author}
            </li>
          ))}
        </ol>
        <hr />
        <a
          href="https://gitlab.com/legislative-tech/myrep-app"
          target="_blank"
          rel="noreferrer"
          className={styles.card}
        >
          <h3>Help build it &rarr;</h3>
          <p>Head on over to the repo and implement some features!</p>
        </a>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
      </footer>
    </div>
  )
}

export async function getServerSideProps(context) {
  const session = await getSession(context)
  return {
    props: { session },
  }
}
