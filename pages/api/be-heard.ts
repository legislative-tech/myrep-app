import { getSession } from 'next-auth/client'
import opentelemetry from '@opentelemetry/api'
import cache from 'memory-cache'

export default async (req, res) => {
  const session = await getSession({ req })
  const span = opentelemetry.trace.getTracer('default').startSpan('api/be-heard')
  span.setAttribute('myrep.tier', 'backend')

  let stories = [
    { id: 1, author: 'jim', story: 'I ate too much ham.' },
    { id: 2, author: 'bob', story: 'Not enough time for ham.' },
  ]
  let next_id = 3
  if (cache.size > 0) {
    console.log('found cache')
    span.setAttribute('myrep.story_cache', 'found')
    stories = JSON.parse(cache.get('stories'))
    next_id = cache.get('next_id')
  }
  span.setAttribute('story-count', stories.length)

  if (session) {
    // Signed in
    span.setAttribute('myrep.auth-status', 'authorized')
    span.setAttribute('myrep.user', session.user.email)

    console.log('Session', JSON.stringify(session, null, 2))

    const story = req.body.story
    if (story) {
      console.log('Story', story)
      stories.push({ id: next_id, author: session.user.name, story: story })
      next_id += 1
      cache.put('next_id', next_id)
      cache.put('stories', JSON.stringify(stories))
      res.statusCode = 200
      span.setAttribute('myrep.story-length', story.length)
    } else {
      span.setAttribute('myrep.story-length', 0)
      res.statusCode = 400
    }
    const words = story.split(' ').length
    span.setAttribute('status_code', res.statusCode)
    res.json({ user: session.email, story: story, word_count: words, stories: stories })
  } else {
    // Not Signed in
    span.setAttribute('myrep.auth-status', 'no session')
    span.setAttribute('status_code', 401)
    res.status(401)
  }
  console.log(JSON.stringify(stories, null, 2))
  span.end()
  res.end()
}
