import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Page.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>MyRep.app terms of service</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          <Link href="/">MyRep.app</Link>
        </h1>

        <p className={styles.description}>Be heard; disagree politely; participate in democracy.</p>

        <div className={styles.legalnonsenseblock}>
          <h3>Terms of Service</h3>
          <p>
            This software is provided under the MIT license. It is an experiment in using technology
            and transparency in a way that is novel and will produce new outcomes. It would be nice
            if the new outcomes were better than what we have now.
          </p>
          <p>
            Any running instance of this software is supported at the discretion of the operator.
          </p>
          <p>
            Acceptable use: Do not do anything illegal or abusive. Do not do security testing on the
            application without the operator&rsquo;s written consent. Do not use the system to send
            spam or distribute copyrighted works.
          </p>
        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
      </footer>
    </div>
  )
}
