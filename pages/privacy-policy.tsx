import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Page.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>MyRep.app privacy policy</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          <Link href="/">MyRep.app</Link>
        </h1>

        <p className={styles.description}>Be heard; disagree politely; participate in democracy.</p>

        <div className={styles.legalnonsenseblock}>
          <h3>Privacy Policy</h3>
          <p>
            This software is provided under the MIT license. It is an experiment in using technology
            and transparency in a way that is novel and will produce new outcomes. It would be nice
            if the new outcomes were better than what we have now.
          </p>
          <p>
            Any running instance of this software is supported at the discretion of the operator.
          </p>
          <p>
            Privacy is a cornerstone of engaging in a free and open dialog to build a better
            democracy. MyRep.app was built with this privacy in mind.
          </p>
          <p>
            Data sent to the application and patterns of user behavior are not part of a deal to
            build a model of the users and monetize it. The data is used by users to communicate.
            The behavior patterns are used by the product team and support team to ensure proper
            system functioning.
          </p>
          <p>
            MyRep.app uses OpenTelemetry to measure system performance and user experience in many
            ways. This data may processed by third parties, depending on the host. This data will
            likely contain user identifiers and other production information. It will be handled
            with the same rigor as the data entered.
          </p>
          <p>I&rsquo;m sure cookies are used all over this thing.</p>
          <p>
            Authentication is provided by SSO providers such as GitLab and Twitter. If you choose to
            sign in using those mechanisms, those providers will know of your account and access to
            the system. The &ldquo;email&rdquo; login method doesn&rsquo;t involve those providers
            but does use an SMTP service provider which is unencrypted.
          </p>
        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
      </footer>
    </div>
  )
}
