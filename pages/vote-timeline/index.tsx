import Head from 'next/head'
import Link from 'next/link'
import styles from '../../styles/Page.module.css'

export default function BeHeard() {
  return (
    <div className={styles.container}>
      <Head>
        <title>My rep&rsquo;s votes</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Link href="/">&larr; Home</Link>
        <h1 className={styles.title}>Vote Timeline</h1>

        <p className={styles.description}>
          The <span className={styles.code}>Vote Timeline</span> section is both a log and forecast
          for votes.
        </p>
        <p className={styles.description}>
          Has the representative voted in alignment with the needs of the district?
        </p>
        <p className={styles.description}>
          How does the representative plan on voting in upcoming votes?
        </p>
        <p className={styles.description}>
          This will relate votes to issues and commentary from the constituency.
        </p>

        <a
          href="https://gitlab.com/legislative-tech/myrep-app"
          target="_blank"
          rel="noreferrer"
          className={styles.card}
        >
          <h3>Help build it &rarr;</h3>
          <p>Head on over to the repo and implement some features!</p>
        </a>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
      </footer>
    </div>
  )
}
