import Head from 'next/head'
import Link from 'next/link'
import styles from '../../styles/Page.module.css'

export default function DistrictIssues() {
  return (
    <div className={styles.container}>
      <Head>
        <title>My reps district&rsquo;s issues</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Link href="/">&larr; Home</Link>
        <h1 className={styles.title}>District Issues</h1>

        <p className={styles.description}>
          The <span className={styles.code}>District Issues</span> section is where you can go to
          learn about your neighbors.
        </p>
        <p className={styles.description}>
          What are common issues that impact many in the community?
        </p>
        <p className={styles.description}>
          What are uncommon but severe issues impacting some in and around the district?
        </p>
        <p className={styles.description}>
          Where can legislative action be brought in to improve these people&rsquo;`s lives?
        </p>

        <a
          href="https://gitlab.com/legislative-tech/myrep-app"
          target="_blank"
          rel="noreferrer"
          className={styles.card}
        >
          <h3>Help build it &rarr;</h3>
          <p>Head on over to the repo and implement some features!</p>
        </a>
      </main>

      <footer className={styles.footer}>
        <a href="https://legislative.tech" target="_blank" rel="noreferrer">
          Powered by Legislative.Tech
        </a>
      </footer>
    </div>
  )
}
