const grpc = require('grpc')
const { NodeTracerProvider } = require("@opentelemetry/node");
const { SimpleSpanProcessor } = require('@opentelemetry/tracing')
const { registerInstrumentations } = require('@opentelemetry/instrumentation')
const { CollectorTraceExporter } = require('@opentelemetry/exporter-collector-grpc')

const provider = new NodeTracerProvider()

if(!process.env.HONEYCOMB_DATASET || !process.env.HONEYCOMB_TEAM){
  console.log("Telemetry not started, running anyway, good luck.")
}
else { 
  console.log("Tracking to honeycomb")
  // guidance from https://docs.honeycomb.io/getting-data-in/opentelemetry/ 
  const metadata = new grpc.Metadata();
  metadata.set('x-honeycomb-team', process.env.HONEYCOMB_TEAM);
  metadata.set('x-honeycomb-dataset', process.env.HONEYCOMB_DATASET);

  const collectorOptions = {
    serviceName: 'myrep-app-service',
    url: 'api.honeycomb.io:443', 
    credentials: grpc.credentials.createSsl(),
    metadata
  }
  const exporter = new CollectorTraceExporter(collectorOptions)
  provider.addSpanProcessor(new SimpleSpanProcessor(exporter))
}
provider.register()
registerInstrumentations({
  tracerProvider: provider,
});