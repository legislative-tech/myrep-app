# Telemetry MyRep.app

The purpose of this telemetry is to be able to answer the question: "is myrep.app running properly"

Sub-questions to be sure the answer is "yes" include:

- "Are users experiencing any slowness?"
- "Is the application recovering itself from continual failures?"
- "Are any backend or internal services struggling?"
- "Are services scaled a reasonable amount?"
- "Is storage expanding wildly?"
- "Are there any anomalies currently happening?"
- "I just deployed new stuff, is it okay?"
- "I just enabled a new feature, is it okay?"

The problematic nature of the word "Telemetry" is from the tech giant's insistance on monetizing it and using it to map personal behaviors. That's not great for many sorts of systems, but completely unacceptable for a system like this. 

Telemetry is included for ops support and it's the responsibility of the system owner to aim it at a responsible collector and handle the data as though it's the same as user data. It is user data. Users in this case are constituents who expect not to be exploited. 

## What telemetry is implemented?

The application has OpenTelemetry built in so that a variety of exporters and collectors can be used.

The first/easiest to be implemented is Honeycomb.io. The user experience on ensuring proper operation is why it was selected. Any contrubtions to make this better, more flexible, etc are welcome.  

## For a real deployment

Since the deployment mechanism is TBD, it may be part of a helm chart or TF file. 

Vercel appears to collect up their own monitoring/metrics so it may handle some of the easies questions. 

