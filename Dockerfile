FROM node:14-slim

RUN mkdir -p /usr/src && \
    apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y ca-certificates && \
    apt-get clean

WORKDIR /usr/src
COPY . /usr/src

RUN yarn install --production && \
    yarn build

EXPOSE 3000 
CMD /usr/src/entrypoint.sh